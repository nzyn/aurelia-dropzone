# Clone
```
git clone https://gitlab.com/nzyn/aurelia-dropzone.git
```

# Install and run client
```
cd aurelia-dropzone
npm install
au run --watch
```

# Install and run test-api
```
cd aurelia-dropzone/test-api
npm install
npm run start
```

# Uploaded files
Uploaded files can be found in `aurelia-dropzone/test-api/uploads`.