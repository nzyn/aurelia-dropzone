import { RouterConfiguration, RouteConfig } from "aurelia-router";
import { PLATFORM } from "aurelia-pal";

export class App {
  router: RouteConfig;

  configureRouter(config: RouterConfiguration, router: RouteConfig) {
    config.title = "Drag N Drop";

    config.map([
      { route: "", name: "home", title: "Home", moduleId: PLATFORM.moduleName("modules/home/home") }
    ]);

    this.router = router;
  }
}
