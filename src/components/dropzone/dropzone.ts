import { bindable } from "aurelia-framework";

export class Dropzone {
  @bindable files: File[] = [];

  dropZone: Element;
  hover: boolean = false;

  get fileApiSupported(): boolean {
    return window.File && window.FileReader && window.FileList && window.Blob;
  }

  attached() {
    // Setup the dnd listeners.
    if (this.fileApiSupported) {
      this.dropZone.addEventListener("dragover", this.handleDragOver.bind(this), false);
      this.dropZone.addEventListener("dragleave", this.handleDragEnd.bind(this), false);
      this.dropZone.addEventListener("drop", this.handleFileSelect.bind(this), false);
      this.dropZone.addEventListener("change", this.handleFileSelect.bind(this), false);
    }
  }

  handleDragOver(e) {
    e.stopPropagation();
    e.preventDefault();
    this.hover = true;
  }

  handleDragEnd(e) {
    e.stopPropagation();
    e.preventDefault();
    this.hover = false;
  }

  handleFileSelect(e) {
    e.stopPropagation();
    e.preventDefault();
    console.log(e);

    let files;
    if (e.dataTransfer)
      files = e.dataTransfer.files;
    else
      files = e.target["files"];

    this.files.push(files[0]);
    this.hover = false;

    console.log(this.files);
  }
}