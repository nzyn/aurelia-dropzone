import { autoinject, bindable } from "aurelia-framework";
import { HttpClient, json } from "aurelia-fetch-client";

@autoinject
export class Home {
  files: File[] = [];

  constructor(
    private http: HttpClient
  ) {
    
  }

  async postFiles() {
    const formData = new FormData();
    for (let i = 0; i < this.files.length; i++) {
      let file = this.files[i];
      formData.append("files", file);
    }

    const response = await this.http.fetch("http://localhost:3000/files", {
      method: "POST",
      body: formData,
      headers: {
        "Accept": "application/json"
      }
    });

    const result = await response.json();
    alert(result.message);
  }
}